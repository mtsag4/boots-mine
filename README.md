Bienvenue,

Si vous recherchez un site web pour votre serveur **Minecraft** entièrement Open Source, vous êtes au bon endroit !

Ce site a été réalisé sous le framework **Bootstrap**, pour le moment la partie "Contact" n'est pas encore fonctionnel. Mais si vous souhaitez travailler sur le projet afin de rendre cette partie utilisable, n'hésiter pas à me contacter ;)

Il se peut que ce site ne soit pas parfait, je suis un développeur web novice et malheureusement ou heureusement j'apprends de mes erreurs.

Si vous rencontrez des bugs ou autres n'hésitez pas à m'en faire part !

J'espère que celui-ci vous sera utile et je vous souhaite de réussir dans votre projet Minecraft ! 👍

**Vidéo de présentation du projet :** _A venir !_